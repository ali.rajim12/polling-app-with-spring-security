package com.rajim.springsecuritypollapp.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Setter
@Getter
@ToString
public class ChoiceRequest {

	@NotBlank
	@Size(max = 40)
	private String text;
}
