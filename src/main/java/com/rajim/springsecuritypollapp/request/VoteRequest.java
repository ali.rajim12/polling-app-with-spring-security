package com.rajim.springsecuritypollapp.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
public class VoteRequest {

	@NotNull
	private Long choiceId;
}
