package com.rajim.springsecuritypollapp.service;

import com.rajim.springsecuritypollapp.model.Poll;
import com.rajim.springsecuritypollapp.request.PollRequest;
import com.rajim.springsecuritypollapp.request.VoteRequest;
import com.rajim.springsecuritypollapp.response.PagedResponse;
import com.rajim.springsecuritypollapp.response.PollResponse;
import com.rajim.springsecuritypollapp.security.UserPrincipal;

public interface PollService {
	PagedResponse<PollResponse> getPollsCreatedBy(String username, UserPrincipal currentUser,
	                                              int page, int size);

	PagedResponse<PollResponse> getPollsVotedBy(String username, UserPrincipal currentUser,
	                                            int page, int size);

	PagedResponse<PollResponse> getAllPolls(UserPrincipal currentUser, int page, int size);

	Poll createPoll(PollRequest pollRequest);

	PollResponse getPollById(Long pollId, UserPrincipal currentUser);

	PollResponse castVoteAndGetUpdatedPoll(Long pollId, VoteRequest voteRequest,
	                                       UserPrincipal currentUser);
}
