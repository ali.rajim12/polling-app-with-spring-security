package com.rajim.springsecuritypollapp.model;

import com.rajim.springsecuritypollapp.model.audit.UserDateAudit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.FetchMode;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.FetchMode.*;

@Setter
@Getter
@Entity
@Table(name = "polls")
public class Poll extends UserDateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 140)
	private String question;

	@OneToMany(
			mappedBy = "poll",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true
	)
	@Size(min = 2, max = 6)
	@BatchSize(size = 30)
	private List<Choice> choices = new ArrayList<>();

	@NotNull
	private Instant expirationDateTime;

	public void addChoice(Choice choice) {
		choices.add(choice);
		choice.setPoll(this);
	}

//	public void removeChoice(Choice choice) {
//		choices.remove(choice);
//		choice.setPoll(null);
//	}

}
