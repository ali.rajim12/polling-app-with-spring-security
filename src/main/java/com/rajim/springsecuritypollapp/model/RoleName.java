package com.rajim.springsecuritypollapp.model;

/**
 * Here we may add roles as per our requirements
 */
public enum RoleName {
	ROLE_USER,
	ROLE_ADMIN
}
