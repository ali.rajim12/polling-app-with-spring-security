package com.rajim.springsecuritypollapp.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ChoiceResponse {

	private long id;

	private String text;

	private long voteCount;
}
