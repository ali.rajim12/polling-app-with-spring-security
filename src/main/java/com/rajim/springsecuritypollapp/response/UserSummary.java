package com.rajim.springsecuritypollapp.response;

import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserSummary {

	private Long id;
	private String username;
	private String name;
}
