package com.rajim.springsecuritypollapp.response;

import lombok.*;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserProfile {

	private Long id;
	private String username;
	private String name;
	// here Instant is taken from java 8 date time Apis
	private Instant joinedAt;
	private Long pollCount;
	private Long voteCount;
}
