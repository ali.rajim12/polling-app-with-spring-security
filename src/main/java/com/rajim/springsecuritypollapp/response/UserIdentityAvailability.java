package com.rajim.springsecuritypollapp.response;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserIdentityAvailability {

	private Boolean available;
}

