package com.rajim.springsecuritypollapp.repository;

import com.rajim.springsecuritypollapp.model.Role;
import com.rajim.springsecuritypollapp.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleName roleName);
}
