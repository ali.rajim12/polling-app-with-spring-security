package com.rajim.springsecuritypollapp.domain;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ChoiceVoteCount {

	private Long choiceId;

	private Long voteCount;
}
